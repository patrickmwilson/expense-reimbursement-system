# PROJECT NAME

## Project Description

This is a basic expense reimbursement system web application. Employees have the ability to log in and submit new reimbursement requests, which can then be viewed and approved or denied by managers.

## Technologies Used

* Java
* MariaDB
* JDBC
* Javalin
* HTML
* CSS
* Javascript
* JUnit
* Selenium
* Mockito



## Features

List of features ready and TODOs for future development
* Employees can log in and submit reimbursement requests for various types of expenses
* Managers can log in and view pending reimbursements and approve or deny them

## Getting Started
   
git clone https://gitlab.com/patrickmwilson/expense-reimbursement-system.git
- Edit the filepath for logging output within log4j.properties file within src/main/resources for your system
- Deploy the application using Javalin by running the MainDriver class within src/main/java/com/projectone

## Usage

- Navigate to http://localhost:5005/html/index.html in your browser
- To log in as a user, use username: "patrickmwilson", password: "password"
- To log in as a manager, use username: "managerman", password: "password"

