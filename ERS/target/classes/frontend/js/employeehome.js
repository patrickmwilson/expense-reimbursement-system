/**
 * 
 */

window.onload = function() {
	//console.log('js is linked');
	//getSessionUser();
	getReimbursements();;
}

function getSessionUser() {
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState == 4 & xhttp.status == 200) {
			let user = JSON.parse(xhttp.responseText);
			console.log(user);
		}
	}
	
	xhttp.open("GET", "http://localhost:5005/users/session");
	xhttp.send();
}

function getReimbursements() {
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState == 4 & xhttp.status == 200) {
			let reimbursements = JSON.parse(xhttp.responseText);
			const container = document.getElementById("reimbursement-card-container");
			reimbursements.forEach(reimb => {

				var type = reimb.type;
				var status = reimb.status;
				var resolver = `Resolved by: ${reimb.resolver}`;
				var amount = `Amount: ${reimb.amount}`;
				var submitted = `Date Submitted: ${reimb.submitted.substring(0,10)}`;

				var resolver = `Resolved by: ${reimb.resolver}`;

				var amount = `Amount: ${reimb.amount}`;
				var submitted = `Date Submitted: ${reimb.submitted.substring(0,10)}`;
				if(reimb.resolved == null) {
					var newcard = `
						<div class="card" style="width: 18rem; margin:auto;">
  							<div class="card-body">
    							<p class="card-text">${reimb.description}</p>
  							</div>
  							<ul class="list-group list-group-flush">
   								<li class="list-group-item">${amount}</li>
    							<li class="list-group-item">${type}</li>
    							<li class="list-group-item">${status}</li>
								<li class="list-group-item">${submitted}</li>
  							</ul>
						</div>
						<br>
					`;
				} else {
					var resolved = `Date Resolved: ${reimb.resolved.substring(0,10)}`;

					var newcard = `
						<div class="card" style="width: 18rem; margin:auto;">
  							<div class="card-body">
    							<p class="card-text">${reimb.description}</p>
  							</div>
  							<ul class="list-group list-group-flush">
   								<li class="list-group-item">${amount}</li>
    							<li class="list-group-item">${type}</li>
    							<li class="list-group-item">${status}</li>
								<li class="list-group-item">${submitted}</li>
								<li class="list-group-item">${resolved}</li>
								<li class="list-group-item">${resolver}</li>
  							</ul>
						</div>
						<br>
					`;
				}
				/* if(reimb.resolved == null) {
					var newcard = `
						<div class="card" style="width: 18rem; margin:auto;">
  							<div class="card-body">
    							<p class="card-text">${reimb.description}</p>
  							</div>
  							<ul class="list-group list-group-flush">
   								<li class="list-group-item">${amount}</li>
    							<li class="list-group-item">${type}</li>
    							<li class="list-group-item">${status}</li>
								<li class="list-group-item">${submitted}</li>
  							</ul>
						</div>
						<br>
					`;
				} else {
					var resolved = `Date Resolved: ${reimb.resolved.substring(0,10)}`;

					var newcard = `
						<div class="card" style="width: 18rem; margin:auto;">
  							<div class="card-body">
    							<p class="card-text">${reimb.description}</p>
  							</div>
  							<ul class="list-group list-group-flush">
   								<li class="list-group-item">${amount}</li>
    							<li class="list-group-item">${type}</li>
    							<li class="list-group-item">${status}</li>
								<li class="list-group-item">${submitted}</li>
								<li class="list-group-item">${resolved}</li>
								<li class="list-group-item">${resolver}</li>
  							</ul>
						</div>
						<br>
					`;
				} */
				document.body.insertAdjacentHTML("beforeend",newcard);
			})
		}
	}
	
	xhttp.open("GET", "http://localhost:5005/reimbursements");
	xhttp.send();
}

function updateStatus() {
	document.getElementById('submitstatus').innerText = "Success";
}