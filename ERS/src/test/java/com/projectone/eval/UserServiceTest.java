package com.projectone.eval;

import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.projectone.dao.UserDao;
import com.projectone.model.User;
import com.projectone.service.UserService;

public class UserServiceTest {
	
	@Mock
	private UserDao uDao;
	private UserService testService = new UserService(uDao);
	private User testUser;
	private User testManager;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new UserService(uDao);
		testUser = new User(1, "patrickmwilson", "password", "Patrick", "Wilson", "patrickmwilson@ucla.edu", 1);
		testManager = new User(2, "managerman", "password", "manager", "man", "managerman@revature.net", 2);
		when(uDao.findByUserName("patrickmwilson")).thenReturn(testUser);
		when(uDao.findByUserName("managerman")).thenReturn(testManager);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testGetUserSuccess() {
		assertEquals(testService.getUser("patrickmwilson"), testUser);
	}
	
	@Test(expected = NullPointerException.class)
	public void testGetUserFailure() {
		assertEquals(testService.getUser("asdfasfd"), null);
	}
	
	@Test
	public void testVerifyPasswordSuccess() {
		assertTrue(testService.verifyPassword("patrickmwilson", "password"));
	}
	
	@Test
	public void testVerifyPasswordFailure() {
		assertFalse(testService.verifyPassword("patrickmwilson", "sdfghsdfg"));
	}
	
	@Test
	public void testIsManagerSuccess() {
		assertTrue(testService.isManager("managerman"));
	}
	
	@Test
	public void testIsManagerFailure() {
		assertFalse(testService.isManager("patrickmwilson"));
	}

}
