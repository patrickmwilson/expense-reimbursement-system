package com.projectone.eval;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.projectone.page.EmployeeHomePage;
import com.projectone.page.LoginPage;
import com.projectone.page.ManagerHomePage;

public class ManagerHomePageTest {
	
	private ManagerHomePage page;
	private LoginPage loginPage;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/test/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.loginPage = new LoginPage(driver);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testManagerTitle() {
		loginPage.setUsername("managerman");
		loginPage.setPassword("password");
		loginPage.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/managerhome.html"));
		this.page = new ManagerHomePage(driver);
		assertEquals(page.getHeader(), "Reimbursements");
	}

}
