package com.projectone.eval;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.projectone.page.EmployeeHomePage;
import com.projectone.page.LoginPage;

public class EmployeeHomePageTest {
	
	private EmployeeHomePage page;
	private LoginPage loginPage;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/test/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.loginPage = new LoginPage(driver);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testEmployeeHomeHeader() {
		loginPage.setUsername("patrickmwilson");
		loginPage.setPassword("password");
		loginPage.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/employeehome.html"));
		this.page = new EmployeeHomePage(driver);
		assertEquals(page.getHeader(), "Submit New Reimbursement");
	}
	
	@Test
	public void testSubmitReimbursement() {
		loginPage.setUsername("patrickmwilson");
		loginPage.setPassword("password");
		loginPage.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/employeehome.html"));
		this.page = new EmployeeHomePage(driver);
		
		page.setAmount(100);
		page.setDescription("Selenium test Reimbursement");
		page.setType("Other");
		page.submit();
		
		
		
		assertEquals(1, 1);
	}

}
