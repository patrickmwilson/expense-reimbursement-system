package com.projectone.eval;

import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Matchers.any;
import static org.junit.Assert.assertEquals;

import com.projectone.dao.ErsDBConnection;
import com.projectone.dao.UserDao;
import com.projectone.model.User;

public class UserDaoTest {
	
	@Mock
	private ErsDBConnection edc;
	
	@Mock
	private Connection c;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	
	private User testUser;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(edc.getDBConnection()).thenReturn(c);
		when(c.prepareStatement(any(String.class))).thenReturn(ps);
		
		testUser = new User(1, "patrickmwilson", "password", "Patrick", "Wilson", "patrickmwilson@ucla.edu", 1);
		
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testUser.getUserId());
		when(rs.getString(2)).thenReturn(testUser.getUsername());
		when(rs.getString(3)).thenReturn(testUser.getPassword());
		when(rs.getString(4)).thenReturn(testUser.getFirstName());
		when(rs.getString(5)).thenReturn(testUser.getLastName());
		when(rs.getString(6)).thenReturn(testUser.getEmail());
		when(rs.getInt(7)).thenReturn(testUser.getRoleId());
		when(ps.executeQuery()).thenReturn(rs);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testFindByUsernameSuccess() {
		assertEquals(new UserDao(edc).findByUserName("patrickmwilson").getUsername(), testUser.getUsername());
	}

}
