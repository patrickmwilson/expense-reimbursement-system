package com.projectone.eval;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.projectone.page.LoginPage;

public class LoginPageTest {
	
	private LoginPage page;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/test/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.page = new LoginPage(driver);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testLoginHeader() {
		assertEquals(page.getHeader(), "Login");
	}
	
	@Test
	public void testSuccessfulEmployeeLogin() {
		page.setUsername("patrickmwilson");
		page.setPassword("password");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/employeehome.html"));
		assertEquals("http://localhost:5005/html/employeehome.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testSuccessfulManagerLogin() {
		page.setUsername("managerman");
		page.setPassword("password");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/managerhome.html"));
		assertEquals("http://localhost:5005/html/managerhome.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testFailedLogin() {
		page.setUsername("patrickmwilson");
		page.setPassword("wrongpassword");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/unsuccessfullogin.html"));
		assertEquals("http://localhost:5005/html/unsuccessfullogin.html", driver.getCurrentUrl());
	}

}
