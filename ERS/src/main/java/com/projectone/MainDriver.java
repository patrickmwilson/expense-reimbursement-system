package com.projectone;

import com.projectone.controller.ReimbursementController;
import com.projectone.controller.UserController;
import com.projectone.dao.ErsDBConnection;
import com.projectone.dao.ReimbursementDao;
import com.projectone.dao.UserDao;
import com.projectone.service.ReimbursementService;
import com.projectone.service.UserService;

import io.javalin.Javalin;

public class MainDriver {
	
	public static void main(String[] args) {
		UserController uCon = new UserController(new UserService(new UserDao(new ErsDBConnection())));
		ReimbursementController rCon = new ReimbursementController(new ReimbursementService(new ReimbursementDao(new ErsDBConnection())));
		
		
		
		Javalin app = Javalin.create(config -> {
			config.addStaticFiles("/frontend");
		});
		
		app.start(5005);
		
		app.post("/users/login", uCon.postLogin);
		app.get("/users/session", uCon.getSessionUser);
		app.post("/reimbursements/new", rCon.postNewReimbursement);
		app.get("/reimbursements", rCon.getUserReimbursements);
		app.get("/allreimbursements", rCon.getAllReimbursements);
		app.post("/approvereimbursement", rCon.approveReimbursement);
		app.post("/denyreimbursement", rCon.denyReimbursement);
		
		
		app.exception(NullPointerException.class, (e, ctx) -> {
			ctx.status(404);
			ctx.result("Error 404 not found");
		});
	}

}
