package com.projectone.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class EmployeeHomePage {
	
	private WebDriver driver;
	
	private WebElement header;
	private WebElement amountField;
	private WebElement descriptionField;
	private Select typeSelector;
	private WebElement submitButton;
	private WebElement status;
	
	public EmployeeHomePage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.header = driver.findElement(By.tagName("h4"));
		this.amountField = driver.findElement(By.name("amount"));
		this.descriptionField = driver.findElement(By.name("description"));
		this.typeSelector = new Select(driver.findElement(By.name("type")));
		this.submitButton = driver.findElement(By.id("submitbutton"));
		this.status = driver.findElement(By.id("submitstatus"));
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:5005/html/employeehome.html");
	}
	
	public void setAmount(int amount) {
		this.amountField.clear();
		this.amountField.sendKeys(String.valueOf(amount));
	}
	
	public String getAmount() {
		return this.amountField.getAttribute("value");
	}
	
	public void setDescription(String description) {
		this.descriptionField.clear();
		this.descriptionField.sendKeys(description);
	}
	
	public void setType(String type) {
		this.typeSelector.selectByVisibleText(type);
	}
	
	public String getHeader() {
		return this.header.getText();
	}
	
	public void submit() {
		this.submitButton.click();
	}
	
	public String getStatus() {
		return this.status.getText();
	}
	
	

}
