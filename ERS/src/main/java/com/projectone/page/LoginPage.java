package com.projectone.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	
	private WebDriver driver;
	
	private WebElement header;
	private WebElement usernameField;
	private WebElement passwordField;
	private WebElement submitButton;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.header = driver.findElement(By.tagName("h3"));
		this.usernameField = driver.findElement(By.id("exampleInputEmail1"));
		this.passwordField = driver.findElement(By.id("exampleInputPassword1"));
		this.submitButton = driver.findElement(By.id("submitbutton"));
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:5005/html/index.html");
	}
	
	public void setUsername(String name) {
		this.usernameField.clear();
		this.usernameField.sendKeys(name);
	}
	
	public String getUsername() {
		return this.usernameField.getAttribute("value");
	}
	
	public void setPassword(String pword) {
		this.passwordField.clear();
		this.passwordField.sendKeys(pword);
	}
	
	public String getPassword() {
		return this.passwordField.getAttribute("value");
	}
	
	public String getHeader() {
		return this.header.getText();
	}
	
	public void submit() {
		this.submitButton.click();
	}

}
