package com.projectone.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ManagerHomePage {
	
private WebDriver driver;
	
	private WebElement title;
	//private WebElement submitButton;
	
	public ManagerHomePage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		

		this.title = driver.findElement(By.tagName("h3"));

		//this.submitButton = driver.findElement(By.id("submitbutton"));

	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:5005/html/managerhome.html");
	}
	
	
	public String getHeader() {
		return this.title.getText();
	}
	

}
