package com.projectone.model;

public class ReimbursementType {
	
	private int typeId;
	private String type;
	
	public ReimbursementType() {
		
	}
	
	public ReimbursementType(String type) {
		super();
		this.type = type;
	}
	
	public ReimbursementType(int typeId, String type) {
		super();
		this.typeId = typeId;
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getTypeId() {
		return typeId;
	}

	@Override
	public String toString() {
		return "ReimbursementType [typeId=" + typeId + ", type=" + type + "]";
	}
	
}
