package com.projectone.model;

public class UserRole {
	
	private int roleId;
	private String role;
	
	public UserRole() {
		
	}
	
	public UserRole(String role) {
		super();
		this.role = role;
	}
	public UserRole(int roleId, String role) {
		super();
		this.roleId = roleId;
		this.role = role;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getRoleId() {
		return roleId;
	}

	@Override
	public String toString() {
		return "UserRole [roleId=" + roleId + ", role=" + role + "]";
	}
	
}
