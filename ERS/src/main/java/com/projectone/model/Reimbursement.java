package com.projectone.model;

public class Reimbursement { 
	
	private int reimbursementId;
	private int amount;
	private String submitted;
	private String resolved;
	private String description;
	private int submitterId;
	private String submitter;
	private int resolverId;
	private String resolver;
	private int typeId;
	private String type;
	private int statusId;
	private String status;
	
	public Reimbursement() {
		
	}
	
	public Reimbursement(int amount, String description, int submitterId, int typeId, int statusId) {
		super();
		this.amount = amount;
		this.description = description;
		this.submitterId = submitterId;
		this.typeId = typeId;
		this.statusId = statusId;
	}
	
	public Reimbursement(int amount, String submitted, String resolved, String description,
			int submitterId, int resolverId, int typeId, int statusId) {
		super();
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.submitterId = submitterId;
		this.resolverId = resolverId;
		this.typeId = typeId;
		this.statusId = statusId;
	}
	
	/*
	 * cs.setInt(1, entity.getAmount());
		cs.setString(2, entity.getDescription());
		cs.setInt(3, entity.getSubmitterId());
		cs.setInt(4, entity.getTypeId());
		cs.setInt(5, entity.getStatusId());
	 */
	
	public Reimbursement(int reimbursementId, int amount, String submitted, String resolved, String description,
			int submitterId, int resolverId, int typeId, int statusId) {
		super();
		this.reimbursementId = reimbursementId;
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.submitterId = submitterId;
		this.resolverId = resolverId;
		this.typeId = typeId;
		this.statusId = statusId;
	}
	
	

	public Reimbursement(int reimbursementId, int amount, String submitted, String resolved, String description,
			int submitterId, int resolverId, int typeId, int statusId, String status, String type, String submitter, String resolver) {
		super();
		this.reimbursementId = reimbursementId;
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.submitterId = submitterId;
		this.submitter = submitter;
		this.resolverId = resolverId;
		this.resolver = resolver;
		this.typeId = typeId;
		this.type = type;
		this.statusId = statusId;
		this.status = status;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getSubmitted() {
		return submitted;
	}

	public void setSubmitted(String submitted) {
		this.submitted = submitted;
	}

	public String getResolved() {
		return resolved;
	}

	public void setResolved(String resolved) {
		this.resolved = resolved;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getSubmitterId() {
		return submitterId;
	}

	public void setSubmitterId(int submitterId) {
		this.submitterId = submitterId;
	}

	public int getResolverId() {
		return resolverId;
	}

	public void setResolverId(int resolverId) {
		this.resolverId = resolverId;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public int getReimbursementId() {
		return reimbursementId;
	}
	
	
	public String getSubmitter() {
		return submitter;
	}

	public void setSubmitter(String submitter) {
		this.submitter = submitter;
	}

	public String getResolver() {
		return resolver;
	}

	public void setResolver(String resolver) {
		this.resolver = resolver;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Reimbursement [reimbursementId=" + reimbursementId + ", amount=" + amount + ", submitted=" + submitted
				+ ", resolved=" + resolved + ", description=" + description + ", submitterId=" + submitterId
				+ ", resolverId=" + resolverId + ", typeId=" + typeId + ", statusId=" + statusId + "]";
	}
	
}
