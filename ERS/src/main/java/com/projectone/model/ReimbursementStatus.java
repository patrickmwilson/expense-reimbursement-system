package com.projectone.model;

public class ReimbursementStatus {
	
	private int statusId;
	private String status;
	
	public ReimbursementStatus() {
		
	}
	
	public ReimbursementStatus(String status) {
		super();
		this.status = status;
	}
	
	public ReimbursementStatus(int statusId, String status) {
		super();
		this.statusId = statusId;
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStatusId() {
		return statusId;
	}

	@Override
	public String toString() {
		return "ReimbursementStatus [statusId=" + statusId + ", status=" + status + "]";
	}
	
}
