package com.projectone.service;

import java.util.List;

import com.projectone.dao.ReimbursementDao;
import com.projectone.model.Reimbursement;

public class ReimbursementService {
	
	private ReimbursementDao rDao;
	
	public ReimbursementService() {
		
	}
	
	public ReimbursementService(ReimbursementDao rDao) {
		super();
		this.rDao = rDao;
	}
	
	public void insertReimbursement(int amount, String description, int submitterId, int typeId) {
		Reimbursement reimb = new Reimbursement(amount, description, submitterId, typeId, 1);
		rDao.insert(reimb);
	}
	
	public List<Reimbursement> getUserReimbursements(int userId) {
		List<Reimbursement> reimbursementList = rDao.findAllBySubmitterId(userId);
		return reimbursementList;
	}
	
	public List<Reimbursement> getAllReimbursements() {
		List<Reimbursement> reimbursementList = rDao.findAll();
		System.out.println(reimbursementList);
		return reimbursementList;
	}
	
	public void approveReimbursement(int reimbursementId, int resolverId) {
		rDao.approve(reimbursementId, resolverId);
	}
	
	public void denyReimbursement(int reimbursementId, int resolverId) {
		rDao.deny(reimbursementId, resolverId);
	}
	
}
