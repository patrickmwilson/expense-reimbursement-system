package com.projectone.service;

import com.projectone.dao.UserDao;
import com.projectone.model.User;

public class UserService {
	
	private UserDao uDao;
	
	public UserService() {
		
	}
	
	public UserService(UserDao uDao) {
		super();
		this.uDao = uDao;
	}
	
	public User getUser(String username) {
		User user = uDao.findByUserName(username);
		if(user == null) {
			throw new NullPointerException();
		}
		return user;
	}
	
	public boolean verifyPassword(String username, String password) {
		User user = getUser(username);
		return (user.getPassword().equals(password));
	}
	
	public boolean isManager(String username) {
		User user = getUser(username);
		return (user.getRoleId() == 2);
	}

}
