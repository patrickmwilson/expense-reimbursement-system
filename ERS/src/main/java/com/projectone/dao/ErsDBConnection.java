package com.projectone.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ErsDBConnection {
	
	private String url = "jdbc:mariadb://database-1.cnfgdq0tb20w.us-east-2.rds.amazonaws.com:3306/ersdb";
	private String username = "ersuser";
	private String password = "mypassword";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}

}
