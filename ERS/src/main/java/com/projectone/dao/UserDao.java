package com.projectone.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.projectone.model.User;

public class UserDao {
	
	private ErsDBConnection edc;
	
	public UserDao() {
		
	}
	
	public UserDao(ErsDBConnection edc) {
		super();
		this.edc = edc;
	}
	
	public User findByUserName(String username) {
		User user = null;
		
		try(Connection con = edc.getDBConnection()) {
			String sql = "SELECT * FROM users WHERE username = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.first()) {
				user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

}
