package com.projectone.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.projectone.model.Reimbursement;

public class ReimbursementDao {
	
	private ErsDBConnection edc;
	
	public ReimbursementDao() {
		
	}
	
	public ReimbursementDao(ErsDBConnection edc) {
		super();
		this.edc = edc;
	}
	
	public List<Reimbursement> findAll() {
		List<Reimbursement> reimbursementList = new ArrayList<>();
		
		try(Connection con = edc.getDBConnection()) {
			
			String sql = "SELECT r.reimbursement_id, r.amount, r.submitted, r.resolved, r.description, r.submitter_id, r.resolver_id, r.type_id, r.status_id, rs.reimbursement_status, rt.reimbursement_type, us.username, ur.username "
					+ "FROM reimbursements r "
					+ "LEFT OUTER JOIN reimbursement_statuses rs ON r.status_id = rs.status_id "
					+ "LEFT OUTER JOIN reimbursement_types rt ON r.type_id = rt.type_id "
					+ "LEFT OUTER JOIN users us ON r.submitter_id = us.user_id "
					+ "LEFT OUTER JOIN users ur ON r.resolver_id = ur.user_id;";
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				Reimbursement temp = new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13));
				reimbursementList.add(temp);
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return reimbursementList;
	}
	
	public List<Reimbursement> findAllBySubmitterId(int submitterId) {
		List<Reimbursement> reimbursementList = new ArrayList<>();
		
		try(Connection con = edc.getDBConnection()) {
			
			String sql = "SELECT r.reimbursement_id, r.amount, r.submitted, r.resolved, r.description, r.submitter_id, r.resolver_id, r.type_id, r.status_id, rs.reimbursement_status, rt.reimbursement_type, us.username, ur.username "
					+ "FROM reimbursements r "
					+ "LEFT OUTER JOIN reimbursement_statuses rs ON r.status_id = rs.status_id "
					+ "LEFT OUTER JOIN reimbursement_types rt ON r.type_id = rt.type_id "
					+ "LEFT OUTER JOIN users us ON r.submitter_id = us.user_id "
					+ "LEFT OUTER JOIN users ur ON r.resolver_id = ur.user_id;";
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				Reimbursement temp = new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13));
				if(temp.getSubmitterId() == submitterId) {
					reimbursementList.add(temp);
				}
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return reimbursementList;
	}
	
	public void insert(Reimbursement entity) {
		
		try(Connection con = edc.getDBConnection()) {
			
			//insert_reimbursement(r_amount IN NUMBER(15), r_description IN VARCHAR2(100), r_submitter_id IN NUMBER(15), r_type_id IN NUMBER(15), r_status_id IN NUMBER(15))
			String sql = "{ call insert_reimbursement(?, ?, ?, ?, ?) }";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, entity.getAmount());
			cs.setString(2, entity.getDescription());
			cs.setInt(3, entity.getSubmitterId());
			cs.setInt(4, entity.getTypeId());
			cs.setInt(5, entity.getStatusId());
			cs.executeUpdate();
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void approve(int reimbursementId, int resolverId) {
		
		try(Connection con = edc.getDBConnection()) {
			
			String sql = "{ call approve_reimbursement(?, ?) }";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, reimbursementId);
			cs.setInt(2, resolverId);
			cs.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deny(int reimbursementId, int resolverId) {
		
		try(Connection con = edc.getDBConnection()) {
			
			String sql = "{ call deny_reimbursement(?, ?) }";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, reimbursementId);
			cs.setInt(2, resolverId);
			cs.executeUpdate();
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}

}
