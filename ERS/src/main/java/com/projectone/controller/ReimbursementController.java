package com.projectone.controller;

import java.util.List;

import com.projectone.model.Reimbursement;
import com.projectone.model.User;
import com.projectone.service.ReimbursementService;

import io.javalin.http.Handler;

public class ReimbursementController {
	
	private ReimbursementService rServ;
	
	public ReimbursementController() {
		
	}
	
	public ReimbursementController(ReimbursementService rServ) {
		super();
		this.rServ = rServ;
	}
	
	public Handler postNewReimbursement = (ctx -> {
		System.out.println(ctx.body());
		System.out.println(((User)ctx.sessionAttribute("user")).getUserId());
		
		int submitterId = ((User)ctx.sessionAttribute("user")).getUserId();
		int amount = Integer.parseInt(ctx.formParam("amount"));
		String description = ctx.formParam("description");
		int typeId = Integer.parseInt(ctx.formParam("type"));
		
		rServ.insertReimbursement(amount, description, submitterId, typeId);
		ctx.redirect("/html/employeehome.html");
	});
	
	
	
	public Handler getUserReimbursements = (ctx -> {
		User user = (User)ctx.sessionAttribute("user");
		List<Reimbursement> reimbursementList = rServ.getUserReimbursements(user.getUserId());
		ctx.json(reimbursementList);
	});
	
	public Handler getAllReimbursements = (ctx -> {
		List<Reimbursement> reimbursementList = rServ.getAllReimbursements();
		ctx.json(reimbursementList);
	});
	
	public Handler approveReimbursement = (ctx -> {
		System.out.println(ctx.body());
		int resolverId = ((User)ctx.sessionAttribute("user")).getUserId();
		int reimbursementId = Integer.parseInt(ctx.formParam("reimbursementId"));
		rServ.approveReimbursement(reimbursementId, resolverId);
		ctx.redirect("/html/loading.html");
		ctx.redirect("/html/managerhome.html");
	});
	
	public Handler denyReimbursement = (ctx -> {
		System.out.println(ctx.body());
		int resolverId = ((User)ctx.sessionAttribute("user")).getUserId();
		int reimbursementId = Integer.parseInt(ctx.formParam("reimbursementId"));
		System.out.println(reimbursementId);
		System.out.println(resolverId);
		rServ.denyReimbursement(reimbursementId, resolverId);
		ctx.redirect("/html/loading.html");
		ctx.redirect("/html/managerhome.html");
	});

}
