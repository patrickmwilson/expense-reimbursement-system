package com.projectone.controller;

import org.apache.log4j.Logger;

import com.projectone.model.User;
import com.projectone.service.UserService;

import io.javalin.http.Handler;

public class UserController {
	
	private UserService uServ;
	private final Logger log = Logger.getLogger(UserController.class);
	
	public UserController() {
		
	}
	
	public UserController(UserService uServ) {
		super();
		this.uServ = uServ;
	}
	
	public Handler postLogin = (ctx -> {
		log.info("Attempted login, username: " + ctx.formParam("username"));
		if(uServ.verifyPassword(ctx.formParam("username"), ctx.formParam("password"))) {
			if(uServ.isManager(ctx.formParam("username"))) {
				ctx.sessionAttribute("user", uServ.getUser(ctx.formParam("username")));
				ctx.redirect("/html/managerhome.html");
				System.out.println("Manager verified");
			} else {
				ctx.sessionAttribute("user", uServ.getUser(ctx.formParam("username")));
				ctx.redirect("/html/employeehome.html");
				System.out.println("User verified");
			}
			
		} else {
			ctx.redirect("/html/unsuccessfullogin.html");
			System.out.println("Invalid username or password");
		}
	});
	
	public Handler getSessionUser = (ctx -> {
		User user = (User)ctx.sessionAttribute("user");
		System.out.println(user);
		ctx.json(user);
	});

}
